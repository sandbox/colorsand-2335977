ABOUT MODRST
-------------

Modrst is a very small module for make it easier  to start or restart the module.
For more information, see the Modrst module page.

Installation
------------
Download, unpack the module the usual way.
Enable this module.

Usage
-----

Go to admin/modules, and you can see the "start" or "restart" link on the far right column.
  * By the "start", you can  directly enable the module.
  * By the "restart", you can make the madule from enabled to disabled, uninstall, installed and 
    finally enabled again. It's convenient to test something about the install file. 
